package com.example.h2jsp.converter;

import com.example.h2jsp.dto.CardStatsDTO;
import com.example.h2jsp.dto.WithdrawDTO;
import com.example.h2jsp.entity.CardOperation;
import com.example.h2jsp.entity.CreditCard;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class CardConverter {
    public static CardStatsDTO toCardStatsDTO(CreditCard creditCard, List<CardOperation> operations) {
        return CardStatsDTO.builder()
                .cardId(creditCard.getId())
                .cardNumber(creditCard.getNumber())
                .withdrawDTOList(CardConverter.toWithdrawDTOList(operations))
                .build();
    }

    private static List<WithdrawDTO> toWithdrawDTOList(List<CardOperation> operations) {
        return operations.stream()
                .map(CardConverter::toWithdrawDTO)
                .collect(Collectors.toList());
    }

    private static WithdrawDTO toWithdrawDTO(CardOperation cardOperation) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yy ");
        return WithdrawDTO.builder()
                .balance(cardOperation.getBalance())
                .sum(cardOperation.getSum())
                .time(cardOperation.getTime().format(formatter))
                .build();
    }
}
