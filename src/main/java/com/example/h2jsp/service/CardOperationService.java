package com.example.h2jsp.service;

import com.example.h2jsp.entity.CardOperation;

import java.util.List;

public interface CardOperationService {
    List<CardOperation> getOperations(long cardId);

    CardOperation addNewOperation(CardOperation cardOperation);
}
