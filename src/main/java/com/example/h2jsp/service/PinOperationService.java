package com.example.h2jsp.service;

import com.example.h2jsp.entity.CreditCard;
import com.example.h2jsp.entity.PinOperation;

import java.util.List;

public interface PinOperationService {
    PinOperation saveWrongOperation(CreditCard creditCard, String pin);

    List<PinOperation> findWrongOperations(long cardId);
}
