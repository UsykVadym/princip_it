package com.example.h2jsp.service.impl;

import com.example.h2jsp.entity.CardOperation;
import com.example.h2jsp.repository.CardOperationRepository;
import com.example.h2jsp.service.CardOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardOperationServiceImpl implements CardOperationService {

    @Autowired
    private CardOperationRepository cardOperationRepository;

    @Override
    public List<CardOperation> getOperations(long cardId) {
        return cardOperationRepository.findByCreditCardId(cardId);
    }

    @Override
    public CardOperation addNewOperation(CardOperation cardOperation) {
        return cardOperationRepository.save(cardOperation);
    }
}
