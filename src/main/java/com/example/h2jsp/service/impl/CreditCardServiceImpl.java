package com.example.h2jsp.service.impl;

import com.example.h2jsp.entity.CreditCard;
import com.example.h2jsp.repository.CreditCardRepository;
import com.example.h2jsp.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class CreditCardServiceImpl implements CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;


    @Override
    public CreditCard findByCreditCardNumber(String cardNumber) {
        return creditCardRepository.findByNumber(cardNumber)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public List<CreditCard> getAllCards() {
        return creditCardRepository.findAll();
    }

    @Override
    public CreditCard findCreditCard(long cardId, String pin) {
        return creditCardRepository.findByIdAndPin(cardId, pin)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public CreditCard findCreditCard(long cardId) {
        return creditCardRepository.findOne(cardId);
    }

    @Override
    public CreditCard updateCreditCard(CreditCard creditCard) {
        return creditCardRepository.save(creditCard);
    }
}
