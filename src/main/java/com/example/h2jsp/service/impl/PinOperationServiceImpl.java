package com.example.h2jsp.service.impl;

import com.example.h2jsp.entity.CreditCard;
import com.example.h2jsp.entity.PinOperation;
import com.example.h2jsp.repository.PinOperationRepository;
import com.example.h2jsp.service.PinOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PinOperationServiceImpl implements PinOperationService {

    @Autowired
    private PinOperationRepository pinOperationRepository;

    @Override
    public PinOperation saveWrongOperation(CreditCard creditCard, String pin) {
        return pinOperationRepository.save(PinOperation.builder()
                .creditCard(creditCard)
                .pin(pin)
                .operationTime(LocalDateTime.now())
                .build());
    }

    @Override
    public List<PinOperation> findWrongOperations(long cardId) {
        return pinOperationRepository.findByCreditCardIdAndOperationTimeAfter(cardId, LocalDateTime.now().minusMinutes(5));
    }
}
