package com.example.h2jsp.service;

import com.example.h2jsp.entity.CreditCard;

import java.util.List;

public interface CreditCardService {
    CreditCard findByCreditCardNumber(String cardNumber);

    List<CreditCard> getAllCards();

    CreditCard findCreditCard(long cardId, String pin);

    CreditCard findCreditCard(long cardId);

    CreditCard updateCreditCard(CreditCard creditCard);
}
