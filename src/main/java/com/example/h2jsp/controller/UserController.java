package com.example.h2jsp.controller;

import com.example.h2jsp.entity.User;
import com.example.h2jsp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String addUser(HttpServletRequest request, ModelMap model) {
        String name = request.getParameter("name");
        userRepository.save(User.builder().name(name).build());
        List<User> all = userRepository.findAll();
        model.put("users", all);
        System.out.println(name);
        return "next";
    }

}