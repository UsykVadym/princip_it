package com.example.h2jsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping(path = "/error")
    public String showError(){
        return "wrong";
    }
}
