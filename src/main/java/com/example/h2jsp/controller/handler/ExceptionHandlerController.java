package com.example.h2jsp.controller.handler;

import com.example.h2jsp.dto.exception.ExceptionDTO;
import com.example.h2jsp.exception.LowBalanceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerController {


    @ExceptionHandler(LowBalanceException.class)
    public ResponseEntity<ExceptionDTO> LowBalanceException(LowBalanceException ex) {
        return new ResponseEntity<>(ExceptionDTO.builder()
                .status(HttpStatus.CONFLICT)
                .message(ex.getMessage())
                .build()
                , HttpStatus.CONFLICT);
    }
}