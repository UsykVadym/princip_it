package com.example.h2jsp.controller;

import com.example.h2jsp.converter.CardConverter;
import com.example.h2jsp.dto.CardStatsDTO;
import com.example.h2jsp.dto.WithdrawResultDTO;
import com.example.h2jsp.entity.CardOperation;
import com.example.h2jsp.entity.CreditCard;
import com.example.h2jsp.exception.LowBalanceException;
import com.example.h2jsp.service.CardOperationService;
import com.example.h2jsp.service.CreditCardService;
import com.example.h2jsp.service.PinOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class CardController {


    private CreditCardService creditCardService;
    private PinOperationService pinOperationService;
    private CardOperationService cardOperationService;

    @Autowired
    public CardController(CreditCardService creditCardService, PinOperationService pinOperationService,
                          CardOperationService cardOperationService) {
        this.creditCardService = creditCardService;
        this.pinOperationService = pinOperationService;
        this.cardOperationService = cardOperationService;
    }

    @GetMapping(value = "/")
    public String showAllCards(ModelMap map) {
        List<CreditCard> all = creditCardService.getAllCards();
        map.put("cards", all);
        return "index";
    }

    @GetMapping(value = "/card/{cardId}/balance")
    public String getBalance(@PathVariable("cardId") long cardId, ModelMap modelMap) {
        CreditCard creditCard = creditCardService.findCreditCard(cardId);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        modelMap.put("holder", creditCard.getUser().getName());
        modelMap.put("amount", creditCard.getAmount());
        modelMap.put("cardId", creditCard.getNumber());
        modelMap.put("pin", creditCard.getPin());
        modelMap.put("date", LocalDateTime.now().format(formatter));
        return "/balance";
    }

    @GetMapping(value = "/card/{cardId}/get_cash")
    public String getCash(@PathVariable("cardId") long cardId, ModelMap modelMap) {
        CreditCard creditCard = creditCardService.findCreditCard(cardId);
        modelMap.put("card", creditCard);
        return "/withdrawals";
    }

    @PostMapping(value = "/card/{cardId}/verify")
    public String verifyCard(@PathVariable("cardId") long cardId,
                             HttpServletRequest request, ModelMap modelMap) {
        String pin = request.getParameter("pin");
        if (pinOperationService.findWrongOperations(cardId).size() > 3) {
            modelMap.put("error", "4 time wrong pin, try  5 minutes later");
            return "wrong";
        } else {
            try {
                CreditCard creditCard = creditCardService.findCreditCard(cardId, pin);
                modelMap.put("card", creditCard);
                return "operation";
            } catch (EntityNotFoundException e) {
                CreditCard creditCard = creditCardService.findCreditCard(cardId);
                pinOperationService.saveWrongOperation(creditCard, pin);
                modelMap.put("errorMessage", "wrong Pin Code");
                modelMap.put("card", creditCard);
                return "pin";
            }
        }
    }

    @PostMapping("/card/{cardId}/withdraw")
    public @ResponseBody
    WithdrawResultDTO withdrawMoney(@PathVariable("cardId") long cardId,
                                    @RequestParam(value = "amount") double amount,
                                    ModelMap modelMap) {
        CreditCard creditCard = creditCardService.findCreditCard(cardId);
        if (creditCard.getAmount() < amount) {
            throw new LowBalanceException("not enough money");
        } else {
            creditCard.setAmount(creditCard.getAmount() - amount);
            CreditCard updated = creditCardService.updateCreditCard(creditCard);
            cardOperationService.addNewOperation(CardOperation.builder()
                    .balance(updated.getAmount())
                    .creditCard(updated)
                    .sum(amount)
                    .time(LocalDateTime.now())
                    .build());
            return WithdrawResultDTO.builder()
                    .sum(amount)
                    .balance(updated.getAmount())
                    .build();
        }

    }

    @GetMapping(value = "/card/{cardId}/operation")
    public String getOperation(@PathVariable("cardId") String cardId, ModelMap modelMap) {

        CreditCard creditCard = creditCardService.findByCreditCardNumber(cardId);
        System.out.println(creditCard);
        modelMap.put("card", creditCard);
        return "operation";
    }

    @RequestMapping(value = "/card", method = RequestMethod.POST)
    public String submitCreditCardNumber(HttpServletRequest request, ModelMap modelMap) {
        String cardNumber = request.getParameter("cardNumber");
        try {
            String deleteMask = cardNumber.replaceAll("-", "");
            CreditCard creditCard = creditCardService.findByCreditCardNumber(deleteMask);
            modelMap.put("user", creditCard.getUser());
            modelMap.put("card", creditCard);
            return "pin";

        } catch (EntityNotFoundException e) {
            modelMap.put("error", "no cards found");
            return "wrong";
        }
    }

    @GetMapping(value = "/card/{cardId}/operations")
    public String getCardOperations(@PathVariable("cardId") long cardId,
                                    ModelMap modelMap) {
        CreditCard creditCard = creditCardService.findCreditCard(cardId);
        List<CardOperation> operations = cardOperationService.getOperations(cardId);
        CardStatsDTO cardStatsDTO = CardConverter.toCardStatsDTO(creditCard, operations);
        modelMap.put("report", cardStatsDTO);
        return "/report";
    }
}
