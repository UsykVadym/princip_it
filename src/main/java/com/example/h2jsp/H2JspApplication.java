package com.example.h2jsp;

import com.example.h2jsp.entity.CreditCard;
import com.example.h2jsp.entity.CardOperation;
import com.example.h2jsp.entity.User;
import com.example.h2jsp.repository.CardOperationRepository;
import com.example.h2jsp.repository.CreditCardRepository;
import com.example.h2jsp.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;

@SpringBootApplication
public class H2JspApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(H2JspApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(H2JspApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(UserRepository userRepository, CreditCardRepository creditCardRepository,
                                  CardOperationRepository cardOperationRepository) {
        return (a) -> {
            User ivan = userRepository.save(User.builder().name("Ivan").build());
            User stepan = userRepository.save(User.builder().name("Stepan").build());

            creditCardRepository.save(CreditCard.builder()
                    .user(ivan)
                    .number("1233432176542345")
                    .pin("1234")
                    .amount(542.22)
                    .build());

            CreditCard savedCreditCard = creditCardRepository.save(CreditCard.builder()
                    .user(ivan)
                    .number("554412349546235")
                    .amount(100.00)
                    .pin("1111")
                    .build());

            cardOperationRepository.save(CardOperation.builder()
                    .creditCard(savedCreditCard)
                    .sum(40)
                    .balance(125)
                    .time(LocalDateTime.now().minusMinutes(5))
                    .build());
            cardOperationRepository.save(CardOperation.builder()
                    .creditCard(savedCreditCard)
                    .sum(25)
                    .balance(100)
                    .time(LocalDateTime.now())
                    .build());

            creditCardRepository.save(CreditCard.builder()
                    .user(stepan)
                    .number("9997765347652345")
                    .amount(56.34)
                    .pin("1111")
                    .build());


            creditCardRepository.findAll().forEach(System.out::println);

        };
    }
}
