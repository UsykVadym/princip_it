package com.example.h2jsp.dto;

import lombok.*;

@Getter
@Setter
@Builder
public class WithdrawResultDTO {

    private double balance, sum;
}
