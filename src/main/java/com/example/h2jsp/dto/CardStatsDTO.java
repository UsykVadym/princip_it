package com.example.h2jsp.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class CardStatsDTO {

    private long cardId;
    private String cardNumber;
    List<WithdrawDTO> withdrawDTOList = new ArrayList<>();

}
