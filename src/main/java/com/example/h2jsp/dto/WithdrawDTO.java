package com.example.h2jsp.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class WithdrawDTO {

    private double sum, balance;
    private String time;
}
