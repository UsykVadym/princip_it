package com.example.h2jsp.dto.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExceptionDTO {

    private HttpStatus status;
    private String message, reason;
}
