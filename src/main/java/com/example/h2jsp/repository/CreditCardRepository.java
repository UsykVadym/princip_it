package com.example.h2jsp.repository;

import com.example.h2jsp.entity.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {

    Optional<CreditCard> findByNumber(String number);

    Optional<CreditCard> findByIdAndPin(long id, String pin);
}
