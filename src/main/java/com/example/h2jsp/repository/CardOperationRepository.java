package com.example.h2jsp.repository;

import com.example.h2jsp.entity.CardOperation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardOperationRepository extends JpaRepository<CardOperation, Long> {

    List<CardOperation> findByCreditCardId(long cardId);
}
