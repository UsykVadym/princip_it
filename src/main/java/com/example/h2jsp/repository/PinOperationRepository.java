package com.example.h2jsp.repository;

import com.example.h2jsp.entity.PinOperation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface PinOperationRepository extends JpaRepository<PinOperation, Long> {

    List<PinOperation> findByCreditCardIdAndOperationTimeAfter(long cardId, LocalDateTime now);
 }
