package com.example.h2jsp.repository;

import com.example.h2jsp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
