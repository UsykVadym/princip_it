package com.example.h2jsp.exception;

public class LowBalanceException extends RuntimeException {

    public LowBalanceException() {
    }

    public LowBalanceException(String message) {
        super(message);
    }
}
