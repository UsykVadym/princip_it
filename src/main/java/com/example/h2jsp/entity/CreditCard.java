package com.example.h2jsp.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "credit_card")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String number;

    private String pin;

    private Double amount;

    @ManyToOne
    private User user;
}
