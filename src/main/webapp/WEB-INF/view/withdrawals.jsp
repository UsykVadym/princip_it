<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>

    <link href="<c:url value="/css/main.css"/>" rel="stylesheet"/>
    <script type="text/javascript">

        function clearError() {
            $('#alert1').css("display", "none");
            $('#alert1').addClass("alert-danger");
            $('#alert1').removeClass("alert-danger");
            $('#alert1').removeClass("alert-success");
        }

        $(document).ready(function () {
            $('#clear2').click(function () {
                clearError();
                $('#amount').val('');
            });
        });

        function putCode(button) {
            clearError();
            var buttonValue = button.value;
            console.log(buttonValue);
            var credCardVal = $('#amount').val();
            $('#amount').val(credCardVal + buttonValue);
        };

        function showError(response) {
            var message = JSON.parse(response).message;
            $('#alert1').css("display", "block");
            $('#alert1').addClass("alert-danger");
            $('#amount').val('');
            $('#result2').text(message);
        };

        function showSuccess(data) {
            console.log(data);
            $('#alert1').css("display", "block");
            $('#alert1').addClass("alert-success");
            $('#amount').val('');
            $('#result2').text('You have succesfuly withdraw ' + data.sum + '; Balance: ' + data.balance);
        }

        function withdrawMoney() {
            var amount = $('#amount').val();
            console.log(amount);
            var url = 'http://localhost:8015/card/'+${card.id}+
            '/withdraw?amount=' + amount;
            console.log(url);
            $.ajax({
                type: 'POST',
                contentType: 'application/json',
                url: url,
                data: null,
                dataType: 'json',
                success: function (data) {
                    showSuccess(data);
                    $('#amount').val('');
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                    if (e.status === 409) {
                        showError(e.responseText);
                    }
                }
            });
        }
    </script>
</head>
<body>


<div class="container creditCardContainer">

    <div class="alert" role="alert" style="display: none" id="alert1">
        <strong> <label id="result2"></label></strong>
    </div>


    <div class="form-group mx-sm-3">
        <label for="amount" id="result">enter sum</label>
        <input readonly type="text" class="form-control" id="amount">
    </div>


    <table class="table">
        <tbody>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="1" onclick="putCode(this)">1</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="2" onclick="putCode(this)">2</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="3" onclick="putCode(this)">3</button>
            </td>
            <td>
                <form class="form-inline" action="/card/${card.number}/operation" method="get">
                    <button type="submit" class="btn btn-danger" onclick="submit">Delete</button>
                </form>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="4" onclick="putCode(this)">4</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="5" onclick="putCode(this)">5</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="6" onclick="putCode(this)">6</button>
            </td>
            <td>
                <button type="button" class="btn btn-warning" id="clear2">Clear</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="7" onclick="putCode(this)">7</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="8" onclick="putCode(this)">8</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="9" onclick="putCode(this)">9</button>
            </td>
            <td>
                <button type="submit" class="btn btn-success" onclick="withdrawMoney()">Enter</button>

            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
            </td>
            <td>
                <button class="btn btn-outline-success" value="0" onclick="putCode(this)">0</button>
            </td>
            <td>
            </td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
