<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>

    <link href="<c:url value="/css/main.css"/>" rel="stylesheet"/>
    <script>
    </script>
</head>
<body>
<table class="table">
    <tbody>
    <tr>
        <th>Sum:</th>
        <th>Balance:</th>
        <th>Time: </th>
    </tr>
    <c:forEach items="${report.withdrawDTOList}" var="operation">
        <tr>
            <td>${operation.sum}</td>
            <td>${operation.balance}</td>
            <td>${operation.time}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="container">
    <form class="form-inline" action="/card/${report.cardNumber}/operation" method="get">
        <button type="submit" class="btn btn-info" onclick="submit">Back</button>
        <button type="button" class="btn btn-danger" onclick="window.location='/';"/>
        EXIT</button>
    </form>
</div>

</body>
</html>
