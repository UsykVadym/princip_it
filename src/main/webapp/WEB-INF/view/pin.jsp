<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vadymusyk
  Date: 12.10.17
  Time: 12:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>PinPage</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link href="<c:url value="/css/main.css"/>" rel="stylesheet"/>
    <script>
        $(document).ready(function () {
            $('#clear2').click(function () {

                $('#inputPassword2').val('');
            });
        });

        function putCode(button) {

            var buttonValue = button.value;
            console.log(buttonValue);
            var credCardVal = $('#inputPassword2').val();
            $('#inputPassword2').val(credCardVal + buttonValue);
        }


    </script>
</head>
<body>


<div class="container creditCardContainer">
    <c:choose>
        <c:when test="${errorMessage !=null}">
            <div thid="error" class="alert alert-danger" role="alert">
                    ${errorMessage}
            </div>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>

    <form class="form-inline " action="/card/${card.id}/verify" method="post">
        <div class="row justify-content-md-center ">
            <div class="col center-pill"><p class="form-control-static">Card #: ${card.number}</p></div>
        </div>
        <div class="row justify-content-md-center ">
            <br>
            <div class="col center-pill">
                <p class="form-control-static">Correct pin-code #: ${card.pin}</p></div>
        </div>
        <div class="form-group mx-sm-3">
            <label for="inputPassword2" class="sr-only">enter PIN</label>
            <input readonly type="password" class="form-control" id="inputPassword2" placeholder="pin" name="pin"
                   value="">
        </div>
        <button type="submit" class="btn btn-primary" onclick="submit">Enter</button>
        <button type="button" class="btn btn-warning" id="clear2">Clear</button>
    </form>

    <table class="table">
        <tbody>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="1" onclick="putCode(this)">1</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="2" onclick="putCode(this)">2</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="3" onclick="putCode(this)">3</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="4" onclick="putCode(this)">4</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="5" onclick="putCode(this)">5</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="6" onclick="putCode(this)">6</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="7" onclick="putCode(this)">7</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="8" onclick="putCode(this)">8</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="9" onclick="putCode(this)">9</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>

            </td>
            <td>
                <button class="btn btn-outline-success" value="0" onclick="putCode(this)">0</button>
            </td>
            <td>

            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
