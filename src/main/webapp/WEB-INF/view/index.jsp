<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
    <link href="<c:url value="/css/main.css"/>" rel="stylesheet"/>
    <script>


        String.prototype.toCardFormat = function () {
            return this.replace(/[^0-9]/g, "").substr(0, 16).split("").reduce(cardFormat, "");

            function cardFormat(str, l, i) {
                return str + ((!i || (i % 4)) ? "" : "-") + l;
            }
        };

        $(document).ready(function () {
            $('#creditCardInputForm').keyup(function () {
                $(this).val($(this).val().toCardFormat());
                $('#result').text($(this).val().toCardFormat());
            });

            $('#clear').click(function () {

                $('#creditCardInputForm').val('');
                $('#result').val('');
            });
        });

        function putCode(button) {

            var buttonValue = button.value;
            var credCardVal = $('#creditCardInputForm').val();
            $('#creditCardInputForm').val((credCardVal + buttonValue).toCardFormat());

        }
    </script>
</head>
<body>
<%--<link href="<c:url value="/css/main.css"/>" rel="stylesheet"/>--%>
<div class="container creditCardContainer">
   Test accounts: <br>
    <c:forEach items="${cards}" var="card">
        ${card.number}<br>
    </c:forEach>
    <div class="row justify-content-md-center">

        <h2 class="h-25 ">ATM</h2>
    </div>
    <form method="POST" action="${pageContext.request.contextPath}/card">
        <div class="form-group">
            <input readonly type="text" class="form-control" id="creditCardInputForm"
                   placeholder="enter card number using screen buttons"
                   name="cardNumber" value="">
        </div>
        <label id="result" for="creditCardInputForm"></label>
        <div class="row justify-content-md-center buttonsRow">
            <button type="submit" class="btn btn-primary" onclick="parentNode.submit();">Next</button>
            <button type="button" class="btn btn-warning" id="clear">Clear</button>
        </div>
    </form>


    <table class="table">
        <tbody>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="1" onclick="putCode(this)">1</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="2" onclick="putCode(this)">2</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="3" onclick="putCode(this)">3</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="4" onclick="putCode(this)">4</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="5" onclick="putCode(this)">5</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="6" onclick="putCode(this)">6</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="btn btn-outline-success" value="7" onclick="putCode(this)">7</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="8" onclick="putCode(this)">8</button>
            </td>
            <td>
                <button class="btn btn-outline-success" value="9" onclick="putCode(this)">9</button>
            </td>
        </tr>
        <tr>
            <th scope="row"></th>
            <td>

            </td>
            <td>
                <button class="btn btn-outline-success" value="0" onclick="putCode(this)">0</button>
            </td>
            <td>

            </td>
        </tr>
        </tbody>
    </table>

</div>

</body>
</html>
